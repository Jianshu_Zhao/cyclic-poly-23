#!/bin/sh

[ $# -eq 1 ] || { cat << EOUSE ; exit 1 ; }
  Usage: $0 [major | minor | patch]

Release cyclic-poly-23.
EOUSE

cargo release --execute --no-dev-version $1
